package me.bram0101.jgame.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import javax.swing.JOptionPane;

import me.bram0101.jgame.Launcher;

public class LibLoader {
	
	public static void loadAll() throws Exception {
		loadLWJGL();
	}
	
	public static void loadLWJGL() throws Exception {
		loadNatives();
	}

	private static File nativeFolder;

	public static void loadNatives() throws Exception {
		nativeFolder = new File(Launcher.GAME_DIR, "natives");
		if(!nativeFolder.exists())
			nativeFolder.mkdir();
		String os = System.getProperty("os.name").toLowerCase().trim();
		if (os.contains("windows")) {
			loadWindowsNatives();
		} else if (os.contains("linux")) {
			loadLinuxNatives();
		} else if (os.contains("macosx")) {
			JOptionPane
					.showMessageDialog(
							null,
							"Some of the natives are possibly not available for librarys for this os that this program uses. Some function can crash!",
							"Warning!", JOptionPane.INFORMATION_MESSAGE);
			loadMacNatives();
		} else if (os.contains("sunos")) {
			loadSolarisNatives();
		} else {
			throw new Exception("No natives available for os: "
					+ System.getProperty("os.name"));
		}
		Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {

			@Override
			public void run() {
				delete(nativeFolder);
			}

			public void delete(File f) {
				if (f.isDirectory()) {
					for (File fl : f.listFiles()) {
						delete(fl);
					}
					f.delete();
				} else {
					f.delete();
				}
			}

		}));
		System.setProperty("org.lwjgl.librarypath", nativeFolder.getPath());
	}

	private static void loadWindowsNatives() throws Exception {
		String[] nativeList;
		if (System.getProperty("os.arch").contains("86")) {
			nativeList = new String[] { "lwjgl.dll", "OpenAL32.dll"};
		} else {
			nativeList = new String[] { "lwjgl64.dll", "OpenAL64.dll"};
		}
		if (!nativeFolder.exists()) {
			if (!nativeFolder.mkdir()) {
				throw new Exception("Could not create native folder");
			}
		}
		for (String s : nativeList) {
			InputStream in = LibLoader.class.getClassLoader()
					.getResourceAsStream("windows/" + s);
			File out = new File(nativeFolder, s);
			OutputStream outstream = new FileOutputStream(out);
			try {
				byte[] buffer = new byte[1024];
				int length;
				while ((length = in.read(buffer)) > 0) {
					outstream.write(buffer, 0, length);
				}
			} finally {
				in.close();
				outstream.close();
			}
		}
	}

	private static void loadMacNatives() throws Exception {
		String[] nativeList;
		if (System.getProperty("os.arch").contains("86")) {
			nativeList = new String[] { "liblwjgl.dylib", "openal.dylib" };
		} else {
			nativeList = new String[] { "liblwjgl.dylib", "openal.dylib" };
		}
		if (!nativeFolder.exists()) {
			if (!nativeFolder.mkdir()) {
				throw new Exception("Could not create native folder");
			}
		}
		for (String s : nativeList) {
			InputStream in = LibLoader.class.getClassLoader()
					.getResourceAsStream("macosx/" + s);
			File out = new File(nativeFolder, s);
			OutputStream outstream = new FileOutputStream(out);
			try {
				byte[] buffer = new byte[1024];
				int length;
				while ((length = in.read(buffer)) > 0) {
					outstream.write(buffer, 0, length);
				}
			} finally {
				in.close();
				outstream.close();
			}
		}
	}

	private static void loadLinuxNatives() throws Exception {
		String[] nativeList;
		if (System.getProperty("os.arch").contains("86")) {
			nativeList = new String[] { "liblwjgl.so", "libopenal.so" };
		} else {
			nativeList = new String[] { "liblwjgl64", "libopenal64.so"};
		}
		if (!nativeFolder.exists()) {
			if (!nativeFolder.mkdir()) {
				throw new Exception("Could not create native folder");
			}
		}
		for (String s : nativeList) {
			InputStream in = LibLoader.class.getClassLoader()
					.getResourceAsStream("linux/" + s);
			File out = new File(nativeFolder, s);
			OutputStream outstream = new FileOutputStream(out);
			try {
				byte[] buffer = new byte[1024];
				int length;
				while ((length = in.read(buffer)) > 0) {
					outstream.write(buffer, 0, length);
				}
			} finally {
				in.close();
				outstream.close();
			}
		}
	}

	private static void loadSolarisNatives() throws Exception {
		String[] nativeList;
		if (System.getProperty("os.arch").contains("86")) {
			nativeList = new String[] { "liblwjgl.so", "libopenal.so" };
		} else {
			nativeList = new String[] { "liblwjgl64.so", "libopenal.so" };
		}
		if (!nativeFolder.exists()) {
			if (!nativeFolder.mkdir()) {
				throw new Exception("Could not create native folder");
			}
		}
		for (String s : nativeList) {
			InputStream in = LibLoader.class.getClassLoader()
					.getResourceAsStream("solaris/" + s);
			File out = new File(nativeFolder, s);
			OutputStream outstream = new FileOutputStream(out);
			try {
				byte[] buffer = new byte[1024];
				int length;
				while ((length = in.read(buffer)) > 0) {
					outstream.write(buffer, 0, length);
				}
			} finally {
				in.close();
				outstream.close();
			}
		}
	}

}
