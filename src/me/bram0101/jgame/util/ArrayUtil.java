package me.bram0101.jgame.util;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;

import me.bram0101.jgame.math.Matrix4f;
import me.bram0101.jgame.render.Vertex;

import org.lwjgl.BufferUtils;

public class ArrayUtil {

	public static FloatBuffer toFloatBuffer(Matrix4f value) {
		FloatBuffer buffer = BufferUtils.createFloatBuffer(4 * 4);
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				buffer.put(value.get(i, j));
			}
		}
		buffer.flip();
		return buffer;
	}

	public static IntBuffer toIntBuffer(int... values) {
		IntBuffer buffer = BufferUtils.createIntBuffer(values.length);
		buffer.put(values);
		buffer.flip();
		return buffer;
	}

	public static String[] removeEmptyStrings(String[] tokens) {
		ArrayList<String> result = new ArrayList<String>();
		for (int i = 0; i < tokens.length; i++)
			if (!tokens[i].equals("")) result.add(tokens[i]);
		String[] res = new String[result.size()];
		result.toArray(res);
		return res;
	}

	public static int[] toIntArray(Integer[] data) {
		int[] res = new int[data.length];
		for (int i = 0; i < data.length; i++)
			res[i] = data[i].intValue();
		return res;
	}

	public static int[] add(int[] orig, int[] add) {
		int[] res = new int[orig.length + add.length];

		for (int i = 0; i < orig.length; i++) {
			res[i] = orig[i];
		}
		for (int i = 0; i < add.length; i++) {
			res[i + orig.length] = add[i];
		}

		return res;
	}
	
	public static Vertex[] add(Vertex[] orig, Vertex[] add) {
		Vertex[] res = new Vertex[orig.length + add.length];

		for (int i = 0; i < orig.length; i++) {
			res[i] = orig[i];
		}
		for (int i = 0; i < add.length; i++) {
			res[i + orig.length] = add[i];
		}

		return res;
	}

}
