package me.bram0101.jgame.util;

import java.util.HashMap;
import java.util.Map;

public class ThreadHandler {

	private static Map<String, Thread> threads = new HashMap<String, Thread>();

	public static void addThread(Thread thread) {
		threads.put(thread.getName(), thread);
	}

	public static void removeThread(String name) {
		threads.remove(name);
	}

	public static Thread getThread(String name) {
		return threads.get(name);
	}

	public static Thread createThread(String name, boolean deamon, Runnable r) {
		Thread t = new Thread(r, name);
		addThread(t);
		t.setDaemon(deamon);
		t.start();
		return t;
	}

	public static void stopThread(String name) {
		Thread t = getThread(name);
		t.stop();
		removeThread(name);
	}

}
