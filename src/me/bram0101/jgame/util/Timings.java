package me.bram0101.jgame.util;

import org.lwjgl.Sys;
import org.lwjgl.opengl.Display;

public class Timings {
	private static long lastFrame;
	public static double delta = 1;
	public static double deltaSeconds;
	public static int FPS;
	private static long lastFPS;
	
	public static void update() {
		long time = getTime();
		delta = time - lastFrame;
		deltaSeconds = delta / 1000;
		lastFrame = time;
		if(lastFPS == 0) {
			lastFPS = getTime();
		}
		while(getTime() - lastFPS > 1000) {
			Display.setTitle("JGame : " + FPS + "FPS");
			FPS = 0;
			lastFPS += 1000;
		}
		FPS++;
	}
	
	public static long getTime() {
		return (Sys.getTime() * 1000) / Sys.getTimerResolution();
	}
}
