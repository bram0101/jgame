package me.bram0101.jgame.util;

import org.lwjgl.opengl.Display;

public class WindowUtil {
	
	public static int getWidth() {
		return Display.getWidth();
	}
	
	public static int getHeight() {
		return Display.getHeight();
	}
	
}
