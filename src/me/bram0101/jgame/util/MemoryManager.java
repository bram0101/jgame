package me.bram0101.jgame.util;

import java.text.DecimalFormat;

import me.bram0101.jgame.logger.Logger;

public class MemoryManager {
	
	private static long minFreeRam = 1024 * 1024 * 100;
	
	private static double time;
	private static long updateTime = 1;
	
	public static long getFreeRam() {
		return Runtime.getRuntime().freeMemory();
	}
	
	public static long getUsedRam() {
		return Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
	}
	
	public static long getMaxRam() {
		return Runtime.getRuntime().maxMemory();
	}
	
	public static long getTotalRam() {
		return Runtime.getRuntime().totalMemory();
	}
	
	public static void update() {
		time+=Timings.deltaSeconds;
		if(time >= updateTime){
			time = 0;
			updateTime = 1;
			if(getFreeRam() < minFreeRam) {//this is 100mb of ram
				updateTime = 10;
				Logger.info(MemoryManager.class, "Performing garbage collection");
				Runtime.getRuntime().gc();
				Logger.info(MemoryManager.class, "Done with garbage collection");
			}
			Logger.info(MemoryManager.class, "RAM: " + readableMemorySize(getUsedRam()) + "/" + readableMemorySize(getTotalRam()));
		}
	}
	
	private static String readableMemorySize(long size) {
	    if(size <= 0) return "0";
	    final String[] units = new String[] { "B", "kB", "MB", "GB", "TB" };
	    int digitGroups = (int) (Math.log10(size)/Math.log10(1024));
	    return new DecimalFormat("#,##0.#").format(size/Math.pow(1024, digitGroups)) + " " + units[digitGroups];
	}
	
}
