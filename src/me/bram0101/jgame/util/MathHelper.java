package me.bram0101.jgame.util;

public class MathHelper {
	
	public static int clamp(int v, int mi, int ma) {
		if(v < mi) v = mi;
		if(v > ma) v = ma;
		return v;
	}
	
	public static float clamp(float v, float mi, float ma) {
		if(v < mi) v = mi;
		if(v > ma) v = ma;
		return v;
	}
	
}
