package me.bram0101.jgame;

import me.bram0101.jgame.math.Vector3f;
import me.bram0101.jgame.util.Input;
import me.bram0101.jgame.util.Timings;

import org.lwjgl.input.Keyboard;

public class Camera {

	public static final Vector3f yAxis = new Vector3f(0, 1, 0);

	private Vector3f pos;
	private Vector3f forward;
	private Vector3f up;
	private float yaw;
	private float pitch;
	private float oldYaw;
	private float oldPitch;
	private float fov = 70F;
	private float zNear = 0.1F;
	private float zFar = 1000F;

	public Camera() {
		this(new Vector3f(0, 0, 0), new Vector3f(0, 0, 1), new Vector3f(0, 1, 0));
	}
	
	public Camera(Vector3f pos) {
		this(pos, new Vector3f(0, 0, 1), new Vector3f(0, 1, 0));
	}

	public Camera(Vector3f pos, Vector3f forward, Vector3f up) {
		this.pos = pos;
		this.forward = forward;
		this.up = up;
		forward.normalize();
		up.normalize();
	}

	public void input() {
		float movAmt = (float) Timings.deltaSeconds * 2;
		float rotAmt = (float) Timings.deltaSeconds * 20;

		if (Input.getKey(Keyboard.KEY_W)) move(getAbsoluteForward(), movAmt);
		if (Input.getKey(Keyboard.KEY_S)) move(getAbsoluteForward(), -movAmt);
		if (Input.getKey(Keyboard.KEY_A)) move(getLeft(), movAmt);
		if (Input.getKey(Keyboard.KEY_D)) move(getRight(), movAmt);
		if (Input.getKey(Keyboard.KEY_SPACE)) move(yAxis, movAmt);
		if (Input.getKey(Keyboard.KEY_LSHIFT)) move(yAxis, -movAmt);

		if (Input.isGrabbed()) {
			rotateX(-Input.getMouseD().y * rotAmt);
			rotateY(Input.getMouseD().x * rotAmt);
		}
	}

	public void move(Vector3f dir, float amt) {
		pos.add(dir.clone().mul(amt));
	}

	/** Pitch */
	public void rotateX(float angle) {
		oldPitch = pitch;
		if (pitch + angle >= 89.5F) {
			angle = 89.5F - pitch;
		}
		if (pitch + angle <= -89.5F) {
			angle = -89.5F - pitch;
		}
		pitch += angle;
		Vector3f Haxis = yAxis.cross(forward);
		Haxis.normalize();

		forward.rotate(angle, Haxis);
		forward.normalize();

		up = forward.cross(Haxis);
		up.normalize();
	}

	/** Yaw */
	public void rotateY(float angle) {
		oldYaw = yaw;
		yaw += angle;
		Vector3f Haxis = yAxis.cross(forward);
		Haxis.normalize();

		forward.rotate(angle, yAxis);
		forward.normalize();

		up = forward.cross(Haxis);
		up.normalize();
	}

	public Vector3f getLeft() {
		Vector3f left = forward.cross(up);
		left.normalize();
		return left;
	}

	public Vector3f getRight() {
		Vector3f right = up.cross(forward);
		right.normalize();
		return right;
	}

	public Vector3f getAbsoluteForward() {
		Vector3f Haxis = yAxis.cross(forward);
		Haxis.normalize();
		Vector3f f = Haxis.cross(yAxis);
		f.normalize();
		return f;
	}

	public Vector3f getPos() {
		return pos;
	}

	public void setPos(Vector3f pos) {
		this.pos = pos;
	}

	public Vector3f getForward() {
		return forward;
	}

	public void setForward(Vector3f forward) {
		this.forward = forward;
	}

	public Vector3f getBackward() {
		Vector3f backward = forward;
		backward.inverse();
		return backward;
	}

	public Vector3f getUp() {
		return up;
	}

	public void setUp(Vector3f up) {
		this.up = up;
	}

	public float getFov() {
		return fov;
	}

	public void setFov(float fov) {
		this.fov = fov;
	}

	public float getzNear() {
		return zNear;
	}

	public void setzNear(float zNear) {
		this.zNear = zNear;
	}

	public float getzFar() {
		return zFar;
	}

	public void setzFar(float zFar) {
		this.zFar = zFar;
	}

}
