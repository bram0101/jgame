package me.bram0101.jgame.io;

import static org.lwjgl.opengl.GL11.GL_NEAREST;
import static org.lwjgl.opengl.GL11.GL_RGBA;
import static org.lwjgl.opengl.GL11.GL_RGBA8;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_MAG_FILTER;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_MIN_FILTER;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_WRAP_S;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_WRAP_T;
import static org.lwjgl.opengl.GL11.GL_UNSIGNED_BYTE;
import static org.lwjgl.opengl.GL11.glBindTexture;
import static org.lwjgl.opengl.GL11.glGenTextures;
import static org.lwjgl.opengl.GL11.glTexImage2D;
import static org.lwjgl.opengl.GL11.glTexParameteri;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;

import me.bram0101.jgame.logger.Logger;
import me.bram0101.jgame.math.Vector2f;
import me.bram0101.jgame.math.Vector3f;
import me.bram0101.jgame.render.Mesh;
import me.bram0101.jgame.render.Texture;
import me.bram0101.jgame.render.Vertex;
import me.bram0101.jgame.util.ArrayUtil;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL12;

public class ResourceLoader {

	public static String loadShader(String name) {
		return loadText("shaders/" + name + ".glsl");
	}

	public static String loadText(String file) {
		StringBuilder sb = new StringBuilder();

		BufferedReader br = null;

		try {
			br = new BufferedReader(new InputStreamReader(ResourceLoader.class.getClassLoader().getResourceAsStream(file)));
			String line;
			while ((line = br.readLine()) != null) {
				sb.append(line).append("\n");
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			if (br != null) try {
				br.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return sb.toString();
	}

	public static List<String> loadTextToList(String file) {
		List<String> sb = new ArrayList<String>();

		BufferedReader br = null;

		try {
			br = new BufferedReader(new InputStreamReader(ResourceLoader.class.getClassLoader().getResourceAsStream(file)));
			String line;
			while ((line = br.readLine()) != null) {
				sb.add(line);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			if (br != null) try {
				br.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return sb;
	}

	public static Mesh loadMesh(String fileName) {
		String[] splitArray = fileName.split("\\.");
		String ext = splitArray[splitArray.length - 1];

		if (!ext.equalsIgnoreCase("obj")) {
			Logger.err(ResourceLoader.class, "Error: File format not supported for: " + fileName);
		}

		List<Vertex> vertices = new ArrayList<Vertex>();
		List<Integer> indices = new ArrayList<Integer>();
		List<Vector2f> uvs = new ArrayList<Vector2f>();
		List<String> text = loadTextToList("mesh/" + fileName);

		for (String line : text) {
			String[] tokens = line.split(" ");
			tokens = ArrayUtil.removeEmptyStrings(tokens);
			if (tokens.length == 0 || tokens[0].equals(" "))
				continue;
			else if (tokens[0].equals("v"))
				vertices.add(new Vertex(new Vector3f(Float.valueOf(tokens[1]), Float.valueOf(tokens[2]), Float.valueOf(tokens[3]))));
			else if (tokens[0].equals("f")) {
				indices.add(Integer.parseInt(tokens[1].split("/")[0]) - 1);
				indices.add(Integer.parseInt(tokens[2].split("/")[0]) - 1);
				indices.add(Integer.parseInt(tokens[3].split("/")[0]) - 1);
				if (tokens.length > 4) {
					indices.add(Integer.parseInt(tokens[1].split("/")[0]) - 1);
					indices.add(Integer.parseInt(tokens[3].split("/")[0]) - 1);
					indices.add(Integer.parseInt(tokens[4].split("/")[0]) - 1);
					
					vertices.get(Integer.parseInt(tokens[1].split("/")[0]) - 1).setTexCoord(uvs.get(Integer.parseInt(tokens[1].split("/")[1]) - 1));
					vertices.get(Integer.parseInt(tokens[3].split("/")[0]) - 1).setTexCoord(uvs.get(Integer.parseInt(tokens[3].split("/")[1]) - 1));
					vertices.get(Integer.parseInt(tokens[4].split("/")[0]) - 1).setTexCoord(uvs.get(Integer.parseInt(tokens[4].split("/")[1]) - 1));
				}
				vertices.get(Integer.parseInt(tokens[1].split("/")[0]) - 1).setTexCoord(uvs.get(Integer.parseInt(tokens[1].split("/")[1]) - 1));
				vertices.get(Integer.parseInt(tokens[2].split("/")[0]) - 1).setTexCoord(uvs.get(Integer.parseInt(tokens[2].split("/")[1]) - 1));
				vertices.get(Integer.parseInt(tokens[3].split("/")[0]) - 1).setTexCoord(uvs.get(Integer.parseInt(tokens[3].split("/")[1]) - 1));
			} else if (tokens[0].equals("vt")) {
				uvs.add(new Vector2f(Float.valueOf(tokens[1]), Float.valueOf(tokens[2])));
			}
		}

		Mesh res = new Mesh();
		Vertex[] vertexData = new Vertex[vertices.size()];
		vertices.toArray(vertexData);
		Integer[] indexData = new Integer[indices.size()];
		indices.toArray(indexData);
		res.setVertices(vertexData, ArrayUtil.toIntArray(indexData));

		return res;
	}

	private static Map<String, Texture> textures = new HashMap<String, Texture>();

	public static Texture loadTexture(String fileName) {

		if (textures.containsKey(fileName)) return textures.get(fileName);

		try {

			BufferedImage img = ImageIO.read(ResourceLoader.class.getClassLoader().getResourceAsStream("textures/" + fileName));
			int bytesPerPixel = 3;
			if (img.getColorModel().hasAlpha()) bytesPerPixel = 4;
			int[] pixels = new int[img.getWidth() * img.getHeight()];
			img.getRGB(0, 0, img.getWidth(), img.getHeight(), pixels, 0, img.getWidth());
			ByteBuffer buffer = BufferUtils.createByteBuffer(img.getWidth() * img.getHeight() * bytesPerPixel);
			for (int x = 0; x < img.getWidth(); x++) {
				for (int y = 0; y < img.getHeight(); y++) {
					int pixel = pixels[y * img.getWidth() + x];
					buffer.put((byte) ((pixel >> 16) & 0xFF));
					buffer.put((byte) ((pixel >> 8) & 0xFF));
					buffer.put((byte) (pixel & 0xFF));
					if (img.getColorModel().hasAlpha()) buffer.put((byte) ((pixel >> 24) & 0xFF));
				}
			}
			buffer.flip();
			int textureID = glGenTextures();
			glBindTexture(GL_TEXTURE_2D, textureID);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL12.GL_CLAMP_TO_EDGE);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL12.GL_CLAMP_TO_EDGE);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, img.getWidth(), img.getHeight(), 0, GL_RGBA, GL_UNSIGNED_BYTE, buffer);
			Texture t = new Texture(textureID);
			textures.put(fileName, t);
			return t;

		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return null;
	}

}
