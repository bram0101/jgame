package me.bram0101.jgame;

import me.bram0101.jgame.util.Input;
import me.bram0101.jgame.util.MemoryManager;
import me.bram0101.jgame.util.OpenGLUtil;
import me.bram0101.jgame.util.ThreadHandler;
import me.bram0101.jgame.util.Timings;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.PixelFormat;

public class GameEngine implements Runnable {

	public static boolean RUNNING = false;
	private static boolean pendStop = false;
	public static Game game;

	public static void START() {
		if (!RUNNING) {
			RUNNING = true;
			ThreadHandler.createThread("gameThread", false, new GameEngine());
		}
	}

	public static void STOP() {
		if (RUNNING) {
			RUNNING = false;
			ThreadHandler.stopThread("gameThread");
		}
	}

	public static void pendStop() {
		pendStop = true;
	}

	@Override
	public void run() {
		try {
			PixelFormat pixelFormat = new PixelFormat(8, 8, 0, 8);
			//ContextAttribs contextAtributes = new ContextAttribs(3, 2).withForwardCompatible(true).withProfileCore(true);
			Display.setDisplayMode(new DisplayMode(1280, 720));
			Display.setTitle("JGame");
			Display.setResizable(true);
			Display.create(pixelFormat);
			Keyboard.create();
			Mouse.create();
			game = new Game();
			init();
			while (!pendStop) {
				if (Display.isCloseRequested()) pendStop();
				Timings.update();
				update();
				Display.update();
				Display.sync(60);
			}
			cleanUp();
			Keyboard.destroy();
			Mouse.destroy();
			Display.destroy();
			STOP();
		} catch (Exception ex) {
			ex.printStackTrace();
			STOP();
		}
	}
	
	private void init() {
		OpenGLUtil.init();
		game.init();
	}
	
	private void update() {
		MemoryManager.update();
		OpenGLUtil.clear();
		//render
		game.render();
		
		Input.update();
	}
	
	private void cleanUp() {
		OpenGLUtil.destroy();
	}

}
