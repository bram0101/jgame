package me.bram0101.jgame.render;

import me.bram0101.jgame.io.ResourceLoader;
import me.bram0101.jgame.math.Matrix4f;

public class BasicShader extends Shader {

	public BasicShader() {
		super();

		addVertexShader(ResourceLoader.loadShader("defaultVertex"));
		addFragmentShader(ResourceLoader.loadShader("defaultFragment"));
		compileShader();
		addUniform("transform");
		addUniform("color");
	}
	
	@Override
	public void updateUniforms(Matrix4f worldMatrix, Matrix4f projectedMatrix, Material material) {
		setUniform("transform", projectedMatrix);
		setUniform("color", material.getColor());
	}

}
