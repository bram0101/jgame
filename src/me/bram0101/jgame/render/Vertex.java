package me.bram0101.jgame.render;

import java.nio.FloatBuffer;

import me.bram0101.jgame.math.Vector2f;
import me.bram0101.jgame.math.Vector3f;

import org.lwjgl.BufferUtils;

public class Vertex {

	public static final int SIZE = 8;

	private Vector3f pos;
	private Vector2f texCoord;
	private Vector3f color = new Vector3f(1, 1, 1);

	public Vertex(float x, float y, float z) {
		this(new Vector3f(x, y, z));
	}

	public Vertex(float x, float y, float z, float r, float g, float b) {
		this(new Vector3f(x, y, z), new Vector3f(r, g, b));
	}

	public Vertex(float x, float y, float z, Vector3f color) {
		this(new Vector3f(x, y, z), color);
	}

	public Vertex(Vector3f pos) {
		this(pos, new Vector2f(0, 0));
	}

	public Vertex(Vector3f pos, Vector2f texCoord) {
		this.pos = pos;
		this.texCoord = texCoord;
	}

	public Vertex(Vector3f pos, Vector2f texCoord, Vector3f color) {
		this(pos, texCoord);
		this.color = color;
	}

	public Vertex(Vector3f pos, Vector3f color) {
		this(pos, new Vector2f(0, 0), color);
	}

	public Vector3f getPos() {
		return pos;
	}

	public void setPos(Vector3f pos) {
		this.pos = pos;
	}

	public Vector2f getTexCoord() {
		return texCoord;
	}

	public void setTexCoord(Vector2f texCoord) {
		this.texCoord = texCoord;
	}

	public Vector3f getColor() {
		return color;
	}

	public void setColor(Vector3f color) {
		this.color = color;
	}

	public static FloatBuffer createFlippedBuffer(Vertex[] vertices) {
		FloatBuffer buffer = BufferUtils.createFloatBuffer(vertices.length * SIZE);

		for (int i = 0; i < vertices.length; i++) {
			buffer.put(vertices[i].pos.x);
			buffer.put(vertices[i].pos.y);
			buffer.put(vertices[i].pos.z);
			buffer.put(vertices[i].texCoord.x);
			buffer.put(vertices[i].texCoord.y);
			buffer.put(vertices[i].color.x);
			buffer.put(vertices[i].color.y);
			buffer.put(vertices[i].color.z);
		}

		buffer.flip();

		return buffer;
	}

}
