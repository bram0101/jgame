package me.bram0101.jgame.render;

import me.bram0101.jgame.util.ArrayUtil;

public class MeshData {

	public Vertex[] vertices;
	public int[] indices;

	public MeshData(Vertex[] vertices, int[] indices) {
		this.vertices = vertices;
		this.indices = indices;
	}

	public MeshData() {
		vertices = new Vertex[0];
		indices = new int[0];
	}

	public void add(Vertex[] vertices, int[] indices) {
		vertices = ArrayUtil.add(this.vertices, vertices);
		indices = ArrayUtil.add(this.indices, indices);
	}

	public void add(MeshData data) {
		add(data.vertices, data.indices);
	}

}
