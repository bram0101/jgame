package me.bram0101.jgame.render;

import static org.lwjgl.opengl.GL20.GL_COMPILE_STATUS;
import static org.lwjgl.opengl.GL20.GL_FRAGMENT_SHADER;
import static org.lwjgl.opengl.GL20.GL_LINK_STATUS;
import static org.lwjgl.opengl.GL20.GL_VALIDATE_STATUS;
import static org.lwjgl.opengl.GL20.GL_VERTEX_SHADER;
import static org.lwjgl.opengl.GL20.glAttachShader;
import static org.lwjgl.opengl.GL20.glCompileShader;
import static org.lwjgl.opengl.GL20.glCreateProgram;
import static org.lwjgl.opengl.GL20.glCreateShader;
import static org.lwjgl.opengl.GL20.glGetProgram;
import static org.lwjgl.opengl.GL20.glGetShader;
import static org.lwjgl.opengl.GL20.glGetShaderInfoLog;
import static org.lwjgl.opengl.GL20.glGetUniformLocation;
import static org.lwjgl.opengl.GL20.glLinkProgram;
import static org.lwjgl.opengl.GL20.glShaderSource;
import static org.lwjgl.opengl.GL20.glUniform1f;
import static org.lwjgl.opengl.GL20.glUniform1i;
import static org.lwjgl.opengl.GL20.glUniform3f;
import static org.lwjgl.opengl.GL20.glUniformMatrix4;
import static org.lwjgl.opengl.GL20.glUseProgram;
import static org.lwjgl.opengl.GL20.glValidateProgram;
import static org.lwjgl.opengl.GL32.GL_GEOMETRY_SHADER;

import java.util.HashMap;
import java.util.Map;

import me.bram0101.jgame.logger.Logger;
import me.bram0101.jgame.math.Matrix4f;
import me.bram0101.jgame.math.Vector3f;
import me.bram0101.jgame.util.ArrayUtil;

public class Shader {

	private int program;
	private Map<String, Integer> uniforms;

	public Shader() {
		program = glCreateProgram();
		uniforms = new HashMap<String, Integer>();

		if (program == 0) {
			Logger.err(getClass(), "Shader creation failed: Could not find valid memory location in constructor");
		}
	}

	public void bind() {
		glUseProgram(program);
	}

	public static void unbind() {
		glUseProgram(0);
	}
	
	public void updateUniforms(Matrix4f worldMatrix, Matrix4f projectedMatrix, Material material) {
		
	}

	public void addUniform(String uniform) {
		int uniformLocation = glGetUniformLocation(program, uniform);
		if (uniformLocation == 0xFFFFFFFF) {
			Logger.err(getClass(), "Couldn't find uniform: " + uniform);
			new Exception().printStackTrace();
			System.exit(-1);
		}

		uniforms.put(uniform, uniformLocation);
	}

	public void setUniformi(String uniformName, int value) {
		glUniform1i(uniforms.get(uniformName), value);
	}

	public void setUniformF(String uniformName, float value) {
		glUniform1f(uniforms.get(uniformName), value);
	}

	public void setUniform(String uniformName, Vector3f value) {
		glUniform3f(uniforms.get(uniformName), value.x, value.y, value.z);
	}

	public void setUniform(String uniformName, Matrix4f value) {
		glUniformMatrix4(uniforms.get(uniformName), true, ArrayUtil.toFloatBuffer(value));
	}

	public void addVertexShader(String text) {
		addProgram(text, GL_VERTEX_SHADER);
	}

	public void addGeometryShader(String text) {
		addProgram(text, GL_GEOMETRY_SHADER);
	}

	public void addFragmentShader(String text) {
		addProgram(text, GL_FRAGMENT_SHADER);
	}

	public void compileShader() {
		glLinkProgram(program);
		if (glGetProgram(program, GL_LINK_STATUS) == 0) {
			Logger.err(getClass(), glGetShaderInfoLog(program, 1024));
			System.exit(-1);
		}

		glValidateProgram(program);

		if (glGetProgram(program, GL_VALIDATE_STATUS) == 0) {
			Logger.err(getClass(), glGetShaderInfoLog(program, 1024));
			System.exit(-1);
		}
	}

	private void addProgram(String text, int type) {
		int shader = glCreateShader(type);
		if (shader == 0) {
			Logger.err(getClass(), "Shader creation failed: Could not find valid memory location at adding shader");
			System.exit(-1);
		}

		glShaderSource(shader, text);
		glCompileShader(shader);

		if (glGetShader(shader, GL_COMPILE_STATUS) == 0) {
			Logger.err(getClass(), glGetShaderInfoLog(shader, 1024) + "     on program: " + shader);
			System.exit(-1);
		}

		glAttachShader(program, shader);
	}

}
