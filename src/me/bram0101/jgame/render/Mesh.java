package me.bram0101.jgame.render;

import static org.lwjgl.opengl.GL11.GL_FLOAT;
import static org.lwjgl.opengl.GL11.GL_TRIANGLES;
import static org.lwjgl.opengl.GL11.GL_UNSIGNED_INT;
import static org.lwjgl.opengl.GL11.glDrawElements;
import static org.lwjgl.opengl.GL15.GL_ARRAY_BUFFER;
import static org.lwjgl.opengl.GL15.GL_ELEMENT_ARRAY_BUFFER;
import static org.lwjgl.opengl.GL15.GL_STATIC_DRAW;
import static org.lwjgl.opengl.GL15.glBindBuffer;
import static org.lwjgl.opengl.GL15.glBufferData;
import static org.lwjgl.opengl.GL15.glGenBuffers;
import static org.lwjgl.opengl.GL20.glDisableVertexAttribArray;
import static org.lwjgl.opengl.GL20.glEnableVertexAttribArray;
import static org.lwjgl.opengl.GL20.glVertexAttribPointer;
import me.bram0101.jgame.GameEngine;
import me.bram0101.jgame.io.ResourceLoader;
import me.bram0101.jgame.math.Transform;
import me.bram0101.jgame.math.Vector3f;
import me.bram0101.jgame.util.ArrayUtil;
import me.bram0101.jgame.util.WindowUtil;

public class Mesh {

	private int vbo;
	private int ibo;
	private int size;
	private Material material;
	private Transform transform;
	private Shader shader;

	public Mesh() {
		transform = new Transform();
		Transform.setProjection(GameEngine.game.getCamera().getFov(), WindowUtil.getWidth(), WindowUtil.getHeight(), GameEngine.game.getCamera().getzNear(),
				GameEngine.game.getCamera().getzFar());
		Transform.setCamera(GameEngine.game.getCamera());
		shader = new BasicShader();
		vbo = glGenBuffers();
		ibo = glGenBuffers();
		size = 0;
		material = new Material(ResourceLoader.loadTexture("white.png"), new Vector3f(1, 1, 1));
	}

	public void setVertices(Vertex[] vertices, int[] indices) {
		size = indices.length;

		glBindBuffer(GL_ARRAY_BUFFER, vbo);
		glBufferData(GL_ARRAY_BUFFER, Vertex.createFlippedBuffer(vertices), GL_STATIC_DRAW);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, ArrayUtil.toIntBuffer(indices), GL_STATIC_DRAW);
	}

	public void setVertices(MeshData data) {
		setVertices(data.vertices, data.indices);
	}

	public void draw() {
		shader.bind();
			shader.updateUniforms(transform.getTransformation(), transform.getProjectedTransformation(), getMaterial());
		if (material == null)
			Texture.unbind();
		else if (material.getTexture() == null)
			Texture.unbind();
		else material.getTexture().bind();
		glEnableVertexAttribArray(0);
		glEnableVertexAttribArray(1);
		glEnableVertexAttribArray(2);

		glBindBuffer(GL_ARRAY_BUFFER, vbo);

		glVertexAttribPointer(0, 3, GL_FLOAT, false, Vertex.SIZE * 4, 0);
		glVertexAttribPointer(1, 2, GL_FLOAT, false, Vertex.SIZE * 4, 12);
		glVertexAttribPointer(2, 3, GL_FLOAT, false, Vertex.SIZE * 4, 20);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
		glDrawElements(GL_TRIANGLES, size, GL_UNSIGNED_INT, 0);

		glDisableVertexAttribArray(0);
		glDisableVertexAttribArray(1);
		glDisableVertexAttribArray(2);
	}

	public Material getMaterial() {
		return material;
	}

	public void setMaterial(Material material) {
		this.material = material;
	}

	public Transform getTransform() {
		return transform;
	}

	public Shader getShader() {
		return shader;
	}

}
