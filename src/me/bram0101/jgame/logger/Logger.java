package me.bram0101.jgame.logger;

public class Logger {

	public static void info(Class clazz, String... info) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < info.length; i++)
			sb.append(info[i] + " ");
		System.out.println("[" + clazz.getSimpleName() + "] " + sb.toString());
	}
	
	public static void err(Class clazz, String... info) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < info.length; i++)
			sb.append(info[i] + " ");
		System.err.println("[" + clazz.getSimpleName() + "] " + sb.toString());
	}

}
