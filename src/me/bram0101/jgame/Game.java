package me.bram0101.jgame;

import me.bram0101.jgame.io.ResourceLoader;
import me.bram0101.jgame.math.Vector3f;
import me.bram0101.jgame.render.Material;
import me.bram0101.jgame.render.Mesh;
import me.bram0101.jgame.render.Shader;
import me.bram0101.jgame.util.Input;
import me.bram0101.jgame.util.Timings;
import me.bram0101.jgame.world.Location;
import me.bram0101.jgame.world.World;
import me.bram0101.jgame.world.block.Block;

import org.lwjgl.input.Keyboard;

public class Game {

	private Mesh mesh;
	private Camera camera;
	World w;

	public void init() {
		camera = new Camera(new Vector3f(0,2,-3));
		w = new World("world", 1);
		w.generate();
		//mesh = ResourceLoader.loadMesh("cube.obj");
		mesh = new Mesh();
		mesh.setVertices(w.getMesh());
		mesh.setMaterial(new Material(ResourceLoader.loadTexture("white.png"), new Vector3f(1,1,1)));
		Input.grabMouse();
	}

	public void render() {
		logic();
		mesh.draw();
		Shader.unbind();
	}

	float temp = 0;

	public void logic() {
		input();
		temp += Timings.deltaSeconds;
	}

	public void input() {
		camera.input();
		if (Input.getKeyDown(Keyboard.KEY_ESCAPE)) Input.releaseMouse();
		if (Input.getMouseDown(0)) Input.grabMouse();
	}
	
	public void destroy() {

	}
	
	public Camera getCamera() {
		return camera;
	}

}
