package me.bram0101.jgame.math;

import me.bram0101.jgame.Camera;

public class Transform {

	private static float zNear;
	private static float zFar;
	private static float width;
	private static float height;
	private static float fov;

	private static Camera camera;

	private Vector3f translation;
	private Vector3f rotation;
	private Vector3f scale;
	private Transform parent;

	public Transform() {
		translation = new Vector3f(0, 0, 0);
		rotation = new Vector3f(0, 0, 0);
		scale = new Vector3f(1, 1, 1);
	}

	public Vector3f getTranslation() {
		return translation;
	}

	public Matrix4f getTransformation() {
		if (parent == null) {
			Matrix4f translationMatrix = new Matrix4f().initTranslation(translation.x, translation.y,
					translation.z);
			Matrix4f rotationMatrix = new Matrix4f().initRotation(rotation.x, rotation.y, rotation.z);
			Matrix4f scaleMatrix = new Matrix4f().initScale(scale.x, scale.y, scale.z);
			return translationMatrix.mul(rotationMatrix.mul(scaleMatrix));
		}
		Matrix4f translationMatrix = new Matrix4f().initTranslation(translation.x + parent.translation.x, translation.y + parent.translation.y, translation.z
				+ parent.translation.z);
		Matrix4f rotationMatrix = new Matrix4f().initRotation(rotation.x + rotation.x, rotation.y + rotation.y, rotation.z + rotation.z);
		Matrix4f scaleMatrix = new Matrix4f().initScale(scale.x + scale.x, scale.y + scale.y, scale.z + scale.z);
		return translationMatrix.mul(rotationMatrix.mul(scaleMatrix));
	}

	public Matrix4f getProjectedTransformation() {
		Matrix4f transformationMatrix = getTransformation();
		Matrix4f projectionMatrix = new Matrix4f().initProjection(fov, width, height, zNear, zFar);
		Matrix4f cameraRotation = new Matrix4f().initCamera(camera.getForward(), camera.getUp());
		Matrix4f cameraTranslation = new Matrix4f().initTranslation(-camera.getPos().x, -camera.getPos().y, -camera.getPos().z);

		return projectionMatrix.mul(cameraRotation.mul(cameraTranslation.mul(transformationMatrix)));
	}

	public static void setProjection(float fov, float width, float height, float zNear, float zFar) {
		Transform.fov = fov;
		Transform.width = width;
		Transform.height = height;
		Transform.zNear = zNear;
		Transform.zFar = zFar;
	}

	public void setTranslation(Vector3f translation) {
		this.translation = translation;
	}

	public void setTranslation(float x, float y, float z) {
		this.translation = new Vector3f(x, y, z);
	}

	public Vector3f getRotation() {
		return rotation;
	}

	public void setRotation(Vector3f rotation) {
		this.rotation = rotation;
	}

	public void setRotation(float x, float y, float z) {
		this.rotation = new Vector3f(x, y, z);
	}

	public Vector3f getScale() {
		return scale;
	}

	public void setScale(Vector3f scale) {
		this.scale = scale;
	}

	public void setScale(float x, float y, float z) {
		this.scale = new Vector3f(x, y, z);
	}

	public static Camera getCamera() {
		return camera;
	}

	public static void setCamera(Camera camera) {
		Transform.camera = camera;
	}

	public Transform getParent() {
		return parent;
	}

	public void setParent(Transform parent) {
		this.parent = parent;
	}

}
