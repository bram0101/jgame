package me.bram0101.jgame.math;

public class Vector3f {

	public float x, y, z;

	public Vector3f(float x, float y, float z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	public Vector3f clone() {
		return new Vector3f(x,y,z);
	}

	public float length() {
		return (float) Math.sqrt(x * x + y * y + z * z);
	}

	public Vector3f rotate(float angle, Vector3f axis) {
		float sinHalfAngle = (float)Math.sin(Math.toRadians(angle / 2));
		float cosHalfAngle = (float)Math.cos(Math.toRadians(angle / 2));
		
		float rX = axis.x * sinHalfAngle;
		float rY = axis.y * sinHalfAngle;
		float rZ = axis.z * sinHalfAngle;
		float rW = cosHalfAngle;
		
		Quaternion rotation = new Quaternion(rX,rY,rZ,rW);
		Quaternion w = rotation.mul(this).mul(rotation.conjugate());
		
		x = w.x;
		y = w.y;
		z = w.z;
		
		return this;
	}

	public float dot(Vector3f r) {
		return x * r.x + y * r.y + z * r.z;
	}

	public Vector3f cross(Vector3f r) {
		float x_ = y * r.z - z * r.y;
		float y_ = z * r.x - x * r.z;
		float z_ = x * r.y - y * r.x;

		return new Vector3f(x_, y_, z_);
	}

	public Vector3f normalize() {
		float length = length();
		x /= length;
		y /= length;
		z /= length;
		return this;
	}

	public Vector3f add(Vector3f r) {
		x += r.x;
		y += r.y;
		z += r.z;
		return this;
	}

	public Vector3f add(float r) {
		x += r;
		y += r;
		z += r;
		return this;
	}

	public Vector3f sub(Vector3f r) {
		x -= r.x;
		y -= r.y;
		z -= r.z;
		return this;
	}

	public Vector3f sub(float r) {
		x -= r;
		y -= r;
		z -= r;
		return this;
	}

	public Vector3f mul(Vector3f r) {
		x *= r.x;
		y *= r.y;
		z *= r.z;
		return this;
	}

	public Vector3f mul(float r) {
		x *= r;
		y *= r;
		z *= r;
		return this;
	}

	public Vector3f devide(Vector3f r) {
		x /= r.x;
		y /= r.y;
		z /= r.z;
		return this;
	}

	public Vector3f devide(float r) {
		x /= r;
		y /= r;
		z /= r;
		return this;
	}

	public Vector3f inverse() {
		x = -x;
		y = -y;
		z = -z;
		return this;
	}

}
