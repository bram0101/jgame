package me.bram0101.jgame.math;

public class Vector2f {

	public float x, y;

	public Vector2f(float x, float y) {
		this.x = x;
		this.y = y;
	}

	public Vector2f copy() {
		return new Vector2f(x, y);
	}

	public float length() {
		return (float) Math.sqrt(x * x + y * y);
	}

	public float dot(Vector2f r) {
		return x * r.x + y * r.y;
	}

	public Vector2f normalize() {
		float length = length();
		x /= length;
		y /= length;
		return this;
	}

	public Vector2f rotate(float angle) {
		double rad = Math.toRadians(angle);
		double cos = Math.cos(rad);
		double sin = Math.sin(rad);
		x = (float)(x * cos - y * sin);
		y = (float)( x * sin + y * cos);
		return this;
	}

	public Vector2f add(Vector2f r) {
		x += r.x;
		y += r.y;
		return this;
	}

	public Vector2f add(float r) {
		x += r;
		y += r;
		return this;
	}

	public Vector2f sub(Vector2f r) {
		x -= r.x;
		y -= r.y;
		return this;
	}

	public Vector2f sub(float r) {
		x -= r;
		y -= r;
		return this;
	}

	public Vector2f mul(Vector2f r) {
		x *= r.x;
		y *= r.y;
		return this;
	}

	public Vector2f mul(float r) {
		x *= r;
		y *= r;
		return this;
	}

	public Vector2f devide(Vector2f r) {
		x /= r.x;
		y /= r.y;
		return this;
	}

	public Vector2f devide(float r) {
		x /= r;
		y /= r;
		return this;
	}

	public Vector2f inverse() {
		x = -x;
		y = -y;
		return this;
	}

}
