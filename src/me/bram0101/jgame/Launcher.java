package me.bram0101.jgame;

import java.io.File;
import java.net.URL;

import me.bram0101.jgame.util.LibLoader;

public class Launcher {

	public static File GAME_DIR;

	public static void main(String[] args) {
		try {
			if (args.length >= 2) {
				for (int i = 0; i < args.length; i += 2) {
					if (args[i].equalsIgnoreCase("--DIR")) {
						GAME_DIR = new File(new URL(args[i + 1]).getFile(), ".JGAME");
					}
				}
			}
			if (GAME_DIR == null) {
				GAME_DIR = new File(System.getProperty("user.home"), ".JGAME");
			}
			if(!GAME_DIR.exists())
				GAME_DIR.mkdir();
			LibLoader.loadAll();
			GameEngine.START();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
