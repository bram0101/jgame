package me.bram0101.jgame.world;

public class Location {

	private float x, y, z;

	public Location(float x, float y, float z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public Location(int x, int y, int z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public Location(double x, double y, double z) {
		this.x = Double.valueOf(x).floatValue();
		this.y = Double.valueOf(y).floatValue();
		this.z = Double.valueOf(z).floatValue();
	}

	public float getX() {
		return x;
	}

	public int getBlockX() {
		return (int) x;
	}

	public void setX(float x) {
		this.x = x;
	}

	public float getY() {
		return y;
	}

	public int getBlockY() {
		return (int) y;
	}

	public void setY(float y) {
		this.y = y;
	}

	public float getZ() {
		return z;
	}

	public int getBlockZ() {
		return (int) z;
	}

	public void setZ(float z) {
		this.z = z;
	}

}
