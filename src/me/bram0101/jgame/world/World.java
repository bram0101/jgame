package me.bram0101.jgame.world;

import me.bram0101.jgame.render.MeshData;
import me.bram0101.jgame.world.block.Block;

public class World {

	public static final int WIDTH = 128;
	public static final int HEIGHT = 128;

	private String name;
	private int id;
	private Chunk[] chunks;

	public World(String name, int id) {
		this.name = name;
		this.id = id;
		chunks = new Chunk[(WIDTH / Chunk.WIDTH) * (HEIGHT / Chunk.DEPTH)];
	}

	public Chunk getChunk(int x, int y) {
		return chunks[y * (WIDTH / Chunk.WIDTH) + x];
	}

	public Chunk getChunk(Location loc) {
		return chunks[(loc.getBlockZ() / Chunk.DEPTH) * (WIDTH / Chunk.WIDTH) + (loc.getBlockX() / Chunk.WIDTH)];
	}
	
	public Block getBlock(Location loc) {
		return getChunk(loc).getBlock(loc);
	}
	
	public void setBlock(Location loc, Block b) {
		getChunk(loc).setBlock(loc, b);
	}

	public void generate() {
		for(int x = 0; x < WIDTH / Chunk.WIDTH; x++) {
			for(int z = 0; z < HEIGHT / Chunk.DEPTH; z++) {
				chunks[z * (WIDTH / Chunk.WIDTH) + x] = new Chunk(x,z);
				chunks[z * (WIDTH / Chunk.WIDTH) + x].generate();
			}
		}
	}
	
	public MeshData getMesh() {
		MeshData res = new MeshData();
		for(int x = 0; x < WIDTH / Chunk.WIDTH; x++) {
			for(int z = 0; z < HEIGHT / Chunk.DEPTH; z++) {
				res.add(chunks[z * (WIDTH / Chunk.WIDTH) + x].getMesh());
			}
		}
		return res;
	}

}
