package me.bram0101.jgame.world.block;

import me.bram0101.jgame.render.MeshData;
import me.bram0101.jgame.render.Vertex;
import me.bram0101.jgame.world.Location;

public class Block {

	public Location location;

	public int id;

	public Block(int id, Location location) {
		this.id = id;
		this.location = location;
	}
	
	public MeshData getMesh() {

		BlockMaterials m = BlockMaterials.getFromId(id);

//		Vertex[] vertices = new Vertex[] { new Vertex(-0.5f, -0.5f, 0.5f, m.color), new Vertex(0.5f, -0.5f, 0.5f, m.color),
//				new Vertex(-0.5f, 0.5f, 0.5f, m.color), new Vertex(0.5f, 0.5f, 0.5f, m.color), new Vertex(-0.5f, 0.5f, -0.5f, m.color),
//				new Vertex(0.5f, 0.5f, -0.5f, m.color), new Vertex(-0.5f, -0.5f, -0.5f, m.color), new Vertex(0.5f, -0.5f, -0.5f, m.color) };
		Vertex[] vertices = new Vertex[] { new Vertex(0, 0, 0.5f, m.color), new Vertex(0.5f, 0, 0.5f, m.color),
				new Vertex(0, 0.5f, 0.5f, m.color), new Vertex(0.5f, 0.5f, 0.5f, m.color), new Vertex(0, 0.5f, 0, m.color),
				new Vertex(0.5f, 0.5f, 0, m.color), new Vertex(0, 0, 0, m.color), new Vertex(0.5f, 0, 0, m.color) };

		int[] indices = new int[] {
				0, 1, 2,
				2, 1, 3,
				
				2, 3, 4,
				4, 3, 5,
				
				4, 5, 6,
				6, 5, 7,
				
				6, 7, 0,
				0, 7, 1,
				
				1, 7, 3,
				3, 7, 5,
				
				6, 0, 4,
				4, 0, 2 };

		return new MeshData(vertices, indices);
	}
}
