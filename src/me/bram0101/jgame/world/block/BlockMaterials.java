package me.bram0101.jgame.world.block;

import java.util.HashMap;

import me.bram0101.jgame.math.Vector3f;

public enum BlockMaterials {

	GRASS(1, new Vector3f(1,1,1));

	int id;
	Vector3f color;

	BlockMaterials(int id, Vector3f color) {
		this.id = id;
		this.color = color;
	}

	private static HashMap<Integer, BlockMaterials> byId = new HashMap<Integer, BlockMaterials>();

	static {
		for (BlockMaterials m : values()) {
			byId.put(Integer.valueOf(m.id), m);
		}
	}

	public static BlockMaterials getFromId(int id) {
		return byId.get(Integer.valueOf(id));
	}

}
