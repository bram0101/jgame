package me.bram0101.jgame.world;

import me.bram0101.jgame.render.MeshData;
import me.bram0101.jgame.util.MathHelper;
import me.bram0101.jgame.world.block.Block;

public class Chunk {

	public static final int WIDTH = 16;
	public static final int DEPTH = 16;
	public static final int HEIGHT = 64;

	private int x, y;
	private Block[] blocks;

	public Chunk(int x, int y) {
		this.x = x;
		this.y = y;
		blocks = new Block[WIDTH * DEPTH * HEIGHT];
	}

	public void generate() {
		for (int x = 0; x < WIDTH; x++) {
			for (int z = 0; z < DEPTH; z++) {
				setBlock(x, 0, z, new Block(1, getWorldLocation(x, 0, z)));
			}
		}
	}

	public Location getWorldLocation(int x, int y, int z) {
		return new Location(this.x * WIDTH + x, y, this.y * DEPTH + z);
	}

	public void setBlock(int x, int y, int z, Block b) {
		x = MathHelper.clamp(x, 0, WIDTH);
		y = MathHelper.clamp(y, 0, HEIGHT);
		z = MathHelper.clamp(z, 0, DEPTH);
		blocks[z * WIDTH * HEIGHT + y * WIDTH + x] = b.id == 0 ? null : b;
	}

	public Block getBlock(int x, int y, int z) {
		x = MathHelper.clamp(x, 0, WIDTH);
		y = MathHelper.clamp(y, 0, HEIGHT);
		z = MathHelper.clamp(z, 0, DEPTH);
		return blocks[z * WIDTH * HEIGHT + y * WIDTH + x];
	}

	public Block getBlock(Location loc) {
		return getBlock(loc.getBlockX() / WIDTH, loc.getBlockY(), loc.getBlockZ() / DEPTH);
	}

	public void setBlock(Location loc, Block b) {
		setBlock(loc.getBlockX() / WIDTH, loc.getBlockY(), loc.getBlockZ() / DEPTH, b);
	}

	public MeshData getMesh() {
		MeshData res = new MeshData();
		for (int x = 0; x < WIDTH; x++) {
			for (int y = 0; y < HEIGHT; y++) {
				for (int z = 0; z < DEPTH; z++) {
					if (getBlock(x, y, z) != null) res.add(getBlock(x, y, z).getMesh());
				}
			}
		}
		return res;
	}

}
