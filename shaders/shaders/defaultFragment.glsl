#version 330

in vec2 texCoord0;
in vec3 color0;

uniform vec3 color;
uniform sampler2D sampler;

out vec4 fragColor;

void main() 
{
	vec4 textureColor = texture2D(sampler, texCoord0.xy);
	
	if(textureColor == 0)
		fragColor = vec4(color, 1.0) * vec4(color0, 1.0);
	else
		fragColor = textureColor * vec4(color, 1.0) * vec4(color0, 1.0);
}